using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEditor.UIElements;
using System;

public class CharacterEditor : EditorWindow
{
    string mapName;

    string newCharacterName;
    string newCharacterClassName;
    bool newCharacterBlocksMovement = false;
    int newCharacterMaxHealth;
    int newCharacterHealth;
    bool newCharacterDead;
    Texture2D newCharacterTexture;
    Vector2Int newCharacterPosition;

    static VisualTreeAsset ItemRowTemplate;

    Character currChar;
    List<Character> characters;
    ListView charSelection;

    [MenuItem("Tools/Character Editor")]
    public static void ShowExample()
    {
        CharacterEditor wnd = GetWindow<CharacterEditor>();
        wnd.titleContent = new GUIContent("CharacterEditor");
    }

    private void SetVisualElementBorder(VisualElement element, float width, Color color)
    {
        element.style.borderTopWidth = element.style.borderBottomWidth =
            element.style.borderLeftWidth = element.style.borderRightWidth = width;
        element.style.borderTopColor = element.style.borderBottomColor =
            element.style.borderLeftColor = element.style.borderRightColor = color;
    }

    public void CreateGUI()
    {
        ItemRowTemplate = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>("Assets/Editor/UI Toolkit - UXML/ItemRowTemplate.uxml");

        currChar = ScriptableObject.CreateInstance<Character>();

        // Each editor window contains a root VisualElement object
        VisualElement root = rootVisualElement;

        VisualElement mainPanel = new VisualElement();
        mainPanel.style.flexGrow = 1.0f;
        mainPanel.style.flexDirection = FlexDirection.Column;
        SetVisualElementBorder(mainPanel, 1.0f, Color.black);
        root.Add(mainPanel);

        mainPanel.Add(new Label("Character Map"));

        TextField newCharMapNameTextField = new TextField("Map Name: ");
        newCharMapNameTextField.RegisterCallback<ChangeEvent<string>>(OnCharMapNameChanged);
        mainPanel.Add(newCharMapNameTextField);

        mainPanel.Add(new Label("Character"));

        ObjectField charObjectField = new ObjectField("Texture: ");
        charObjectField.objectType = typeof(Texture);
        charObjectField.RegisterCallback<ChangeEvent<UnityEngine.Object>>(OnCharTextureChanged);
        mainPanel.Add(charObjectField);

        TextField newCharNameTextField = new TextField("Name: ");
        newCharNameTextField.RegisterCallback<ChangeEvent<string>>(OnCharNameChanged);
        mainPanel.Add(newCharNameTextField);

        TextField newCharClassNameTextField = new TextField("Class Name: ");
        newCharClassNameTextField.RegisterCallback<ChangeEvent<string>>(OnCharClassNameChanged);
        mainPanel.Add(newCharClassNameTextField);

        Toggle newCharBlocksMovement = new Toggle("Blocks Movement: ");
        newCharBlocksMovement.RegisterCallback<ChangeEvent<bool>>(OnCharBlocksMovementChanged);
        mainPanel.Add(newCharBlocksMovement);

        IntegerField newCharMaxHealthField = new IntegerField("Max Health: ");
        newCharMaxHealthField.RegisterCallback<ChangeEvent<int>>(OnCharMaxHealthChanged);
        mainPanel.Add(newCharMaxHealthField);

        IntegerField newCharHealthField = new IntegerField("Health: ");
        newCharHealthField.RegisterCallback<ChangeEvent<int>>(OnCharHealthChanged);
        mainPanel.Add(newCharHealthField);

        Toggle newCharDead = new Toggle("Dead: ");
        newCharDead.RegisterCallback<ChangeEvent<bool>>(OnCharDeadChanged);
        mainPanel.Add(newCharDead);

        Vector2IntField newCharPos = new Vector2IntField("Position: ");
        newCharPos.RegisterCallback<ChangeEvent<Vector2Int>>(OnCharPosChanged);
        mainPanel.Add(newCharPos);

        Button saveCharButton = new Button(SaveChar)
        {
            text = "Save Character",
            style =
            {
                width = 100,
                height = 30
            }
        };
        mainPanel.Add(saveCharButton);

        Button saveCharMapButton = new Button(CreateCharMap)
        {
            text = "Save Map",
            style =
            {
                width = 100,
                height = 30
            }
        };
        mainPanel.Add(saveCharMapButton);

        VisualElement charSelectionPanel = new VisualElement();
        charSelectionPanel.style.flexGrow = 0.8f;
        charSelectionPanel.style.flexDirection = FlexDirection.Column;
        SetVisualElementBorder(charSelectionPanel, 1.0f, Color.black);
        mainPanel.Add(charSelectionPanel);

        characters = new List<Character>(Resources.LoadAll<Character>("ScrObjects/Characters/"));

        charSelectionPanel.Add(new Label("Select Character"));

        Func<VisualElement> makeItem = () => ItemRowTemplate.CloneTree();

        Action<VisualElement, int> bindItem = (e, i) =>
        {
            e.Q<VisualElement>("Icon").style.backgroundImage = characters[i] == null ? Resources.Load<Texture2D>("Default") : characters[i].texture;
            e.Q<Label>("Name").text = characters[i].name;
        };

        charSelection = new ListView(characters);
        charSelection.selectionType = SelectionType.Single;
        charSelection.style.flexGrow = 1.0f;
        charSelection.onSelectionChange += x => currChar = charSelection.selectedItem as Character;

        charSelectionPanel.Add(charSelection);
    }

    private void OnCharTextureChanged(ChangeEvent<UnityEngine.Object> _evt)
    {
        newCharacterTexture = _evt.newValue as Texture2D;
    }

    private void OnCharMapNameChanged(ChangeEvent<string> _evt)
    {
        mapName = _evt.newValue;
    }
    private void OnCharNameChanged(ChangeEvent<string> _evt)
    {
        newCharacterName = _evt.newValue;
    }

    private void OnCharClassNameChanged(ChangeEvent<string> _evt)
    {
        newCharacterClassName = _evt.newValue;
    }

    private void OnCharBlocksMovementChanged(ChangeEvent<bool> _evt)
    {
        newCharacterBlocksMovement = _evt.newValue;
    }

    private void OnCharMaxHealthChanged(ChangeEvent<int> _evt)
    {
        newCharacterMaxHealth = _evt.newValue;
    }

    private void OnCharHealthChanged(ChangeEvent<int> _evt)
    {
        newCharacterHealth = _evt.newValue;
    }

    private void OnCharDeadChanged(ChangeEvent<bool> _evt)
    {
        newCharacterDead = _evt.newValue;
    }

    private void OnCharPosChanged(ChangeEvent<Vector2Int> _evt)
    {
        newCharacterPosition = _evt.newValue;
    }

    private void SaveChar()
    {
        Character chara = ScriptableObject.CreateInstance<Character>();

        chara.texture = newCharacterTexture;
        chara.name = newCharacterName;
        chara.className = newCharacterClassName;
        chara.blocksMovement = newCharacterBlocksMovement;
        chara.maxHealth = newCharacterMaxHealth;
        chara.health = newCharacterHealth;
        chara.dead = newCharacterDead;
        chara.position = newCharacterPosition;

        characters.Add(chara);
    }   
    private void CreateCharMap()
    {
        string path = $"Assets/Resources/ScrObjects/CharMaps/{mapName}.asset";

        CharMap charMap = ScriptableObject.CreateInstance<CharMap>();
        charMap.characters = new List<CharStruct>();

        foreach(var chara in characters)
        {
            charMap.characters.Add(new CharStruct(chara));
        }

        try
        {
            AssetDatabase.CreateAsset(charMap, path);
        }
        catch (System.Exception e)
        {
            EditorUtility.DisplayDialog("Error", e.Message, "OK");
            return;
        }
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }
}

using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEditor.UIElements;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System;

public class TilemapEditor : EditorWindow
{
    public enum State
    {
        Draw,
        Erase
    };

    Tile currentTile; // Texture of currently selected tile
    State drawState; // Draw/Erase

    Dictionary<Box, Tile> tileMapDict;

    Texture2D newTileTexture;
    string newTileName;
    string newTileClassName;
    bool newTileBlocksMovement = false;

    string newTileMapName;
    TileMap loadedTileMap;

    private static VisualTreeAsset ItemRowTemplate;
    List<Tile> tiles;
    ListView tileSelection;
    private int ItemHeight = 60;

    Texture2D defaultTexture;

    [MenuItem("Tools/Tilemap Editor")]
    public static void ShowExample()
    {
        TilemapEditor wnd = GetWindow<TilemapEditor>();
        wnd.titleContent = new GUIContent("TilemapEditor");
    }

    private void SetVisualElementBorder(VisualElement element, float width, Color color)
    {
        element.style.borderTopWidth = element.style.borderBottomWidth =
            element.style.borderLeftWidth = element.style.borderRightWidth = width;
        element.style.borderTopColor = element.style.borderBottomColor =
            element.style.borderLeftColor = element.style.borderRightColor = color;
    }


    public void CreateGUI()
    {
        tileMapDict = new Dictionary<Box, Tile>();

        defaultTexture = Resources.Load<Texture2D>("Default");
        ItemRowTemplate = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>("Assets/Editor/UI Toolkit - UXML/ItemRowTemplate.uxml");
        currentTile = new Tile();
        currentTile.name = "defaultName";
        currentTile.texture = defaultTexture;

        // Each editor window contains a root VisualElement object
        VisualElement root = rootVisualElement;

        VisualElement mainPanel = new VisualElement();
        mainPanel.style.flexGrow = 1.0f;
        mainPanel.style.flexDirection = FlexDirection.Row;
        SetVisualElementBorder(mainPanel, 1.0f, Color.black);
        root.Add(mainPanel);

        #region Left Panel
        VisualElement leftPanel = new VisualElement();
        leftPanel.style.flexGrow = 0.33f;
        leftPanel.style.flexDirection = FlexDirection.Column;
        SetVisualElementBorder(leftPanel, 1.0f, Color.black);
        mainPanel.Add(leftPanel);

        #region Tile Creation
        VisualElement tileCreationPanel = new VisualElement();
        tileCreationPanel.style.flexGrow = 0.2f;
        tileCreationPanel.style.flexDirection = FlexDirection.Column;
        SetVisualElementBorder(tileCreationPanel, 1.0f, Color.black);
        tileCreationPanel.Add(new Label("Tile Creation"));
        leftPanel.Add(tileCreationPanel);

        ObjectField tileObjectField = new ObjectField("Texture: ");
        tileObjectField.objectType = typeof(Texture);
        tileObjectField.RegisterCallback<ChangeEvent<UnityEngine.Object>>(OnTileTextureChanged);
        tileCreationPanel.Add(tileObjectField);

        TextField newTileNameTextField = new TextField("Name: ");
        newTileNameTextField.RegisterCallback<ChangeEvent<string>>(OnTileNameChanged);
        tileCreationPanel.Add(newTileNameTextField);

        TextField newTileClassNameTextField = new TextField("Class Name: ");
        newTileClassNameTextField.RegisterCallback<ChangeEvent<string>>(OnTileClassNameChanged);
        tileCreationPanel.Add(newTileClassNameTextField);

        Toggle newTileBlocksMovement = new Toggle("Blocks Movement: ");
        newTileBlocksMovement.RegisterCallback<ChangeEvent<bool>>(OnTileBlocksMovementChanged);
        tileCreationPanel.Add(newTileBlocksMovement);

        Button createTileButton = new Button(CreateNewTile)
        {
            text = "Create New Tile",
            style =
            {
                width = 100,
                height = 30
            }
        };
        tileCreationPanel.Add(createTileButton);
        #endregion

        #region TileMap Save/Load
        VisualElement tileMapSavePanel = new VisualElement();
        tileMapSavePanel.style.flexGrow = 0.1f;
        tileMapSavePanel.style.flexDirection = FlexDirection.Column;
        SetVisualElementBorder(tileCreationPanel, 1.0f, Color.black);
        tileMapSavePanel.Add(new Label("TileMap Save"));
        leftPanel.Add(tileMapSavePanel);

        TextField tileMapNameField = new TextField("TileMap Name: ");
        tileMapNameField.RegisterCallback<ChangeEvent<string>>(OnTileMapNameChanged);
        tileMapSavePanel.Add(tileMapNameField);

        Button saveTileMapButton = new Button(CreateTileMap)
        {
            text = "Save TileMap",
            style =
            {
                width = 100,
                height = 30
            }
        };
        tileMapSavePanel.Add(saveTileMapButton);

        Button exportTileMapButton = new Button(ExportTileMap)
        {
            text = "Export to JSON",
            style =
            {
                width = 100,
                height = 30
            }
        };
        tileMapSavePanel.Add(exportTileMapButton);

        VisualElement tileMapLoadPanel = new VisualElement();
        tileMapLoadPanel.style.flexGrow = 0.1f;
        tileMapLoadPanel.style.flexDirection = FlexDirection.Column;
        SetVisualElementBorder(tileCreationPanel, 1.0f, Color.black);
        tileMapLoadPanel.Add(new Label("TileMap Load"));
        leftPanel.Add(tileMapLoadPanel);

        ObjectField tileMapObjectField = new ObjectField("TileMap: ");
        tileMapObjectField.objectType = typeof(TileMap);
        tileMapObjectField.RegisterCallback<ChangeEvent<UnityEngine.Object>>(OnTileMapObjectChanged);
        tileMapLoadPanel.Add(tileMapObjectField);

        Button loadTileMapButton = new Button(LoadTileMap)
        {
            text = "Load TileMap",
            style =
            {
                width = 100,
                height = 30
            }
        };
        tileMapLoadPanel.Add(loadTileMapButton);

        #endregion

        #region Tile Selection
        VisualElement tileSelectionPanel = new VisualElement();
        tileSelectionPanel.style.flexGrow = 0.6f;
        tileSelectionPanel.style.flexDirection = FlexDirection.Column;
        SetVisualElementBorder(tileSelectionPanel, 1.0f, Color.black);
        leftPanel.Add(tileSelectionPanel);

        EnumField drawingState = new EnumField("Drawing type: ", State.Draw);
        drawingState.RegisterValueChangedCallback<System.Enum>(
            (_evt) =>
            {
               drawState = (State)_evt.newValue;
            }
        );
        tileSelectionPanel.Add(drawingState);

        tiles = new List<Tile>(Resources.LoadAll<Tile>("ScrObjects/Tiles/"));

        tileSelectionPanel.Add(new Label("Select Tile"));

        Func<VisualElement> makeItem = () => ItemRowTemplate.CloneTree();

        Action<VisualElement, int> bindItem = (e, i) =>
        {
            e.Q<VisualElement>("Icon").style.backgroundImage = tiles[i] == null ? defaultTexture : tiles[i].texture;
            e.Q<Label>("Name").text = tiles[i].name;
        };

        tileSelection = new ListView(tiles, ItemHeight, makeItem, bindItem);
        tileSelection.selectionType = SelectionType.Single;
        tileSelection.style.flexGrow = 1.0f;
        tileSelection.style.height = tiles.Count * ItemHeight + 5;
        tileSelection.onSelectionChange += x => currentTile = (tileSelection.selectedItem as Tile);
        tileSelection.SetSelection(0);

        tileSelectionPanel.Add(tileSelection);
        #endregion

        #endregion

        #region Tilemap Panel
        VisualElement tilemapPanel = new VisualElement();
        tilemapPanel.style.flexGrow = 0.34f;
        tilemapPanel.style.flexDirection = FlexDirection.Column;
        SetVisualElementBorder(tilemapPanel, 1.0f, Color.black);
        mainPanel.Add(tilemapPanel);


        // Map layout
        for (int y = 0; y < 10; y++)
        {
            VisualElement rowPanel = new VisualElement();
            rowPanel.style.flexDirection = FlexDirection.Row;
            tilemapPanel.Add(rowPanel);

            for(int x = 0; x < 10; x++)
            {
                Box box = new Box()
                {
                    style =
                    {
                        minWidth = 64,
                        minHeight = 64,
                        maxWidth = 64,
                        maxHeight = 64
                    }
                };
                
                SetVisualElementBorder(box, 1, Color.black);

                box.RegisterCallback<MouseUpEvent>(OnMouseUpEvent);

                Image image = new Image();
                image.style.flexShrink = 1.0f;
                image.image = defaultTexture;
                box.Add(image);

                Tile tempTile = new Tile();
                tempTile.name = "Tile";
                tempTile.className = "Tile";
                tempTile.texture = defaultTexture;
                tempTile.position = new Vector2Int(x, y);

                tileMapDict[box] = tempTile;

                rowPanel.Add(box);
            }
        }
        #endregion
    }
    private void OnTileTextureChanged(ChangeEvent<UnityEngine.Object> _evt)
    {
        newTileTexture = _evt.newValue as Texture2D; 
    }

    private void OnTileNameChanged(ChangeEvent<string> _evt)
    {
        newTileName = _evt.newValue;
    }

    private void OnTileClassNameChanged(ChangeEvent<string> _evt)
    {
        newTileClassName = _evt.newValue;
    }

    private void OnTileBlocksMovementChanged(ChangeEvent<bool> _evt)
    {
        newTileBlocksMovement = _evt.newValue;
    }

    private void OnTileMapNameChanged(ChangeEvent<string> _evt)
    {
        newTileMapName = _evt.newValue;
    }

    private void OnTileMapObjectChanged(ChangeEvent<UnityEngine.Object> _evt)
    {
        loadedTileMap = _evt.newValue as TileMap;
    }

    private void OnMouseUpEvent(MouseUpEvent _event)
    {
        Image image = _event.target as Image;
        Box box = image.parent as Box;

        switch(drawState)
        {
            case State.Draw:
                if (image != null)
                {
                    image.image = currentTile.texture;
                    currentTile.position = tileMapDict[box].position;
                    tileMapDict[box] = currentTile;
                }
                break;

            case State.Erase:
                if (image != null)
                {
                    image.image = defaultTexture;
                    tileMapDict[box].texture = defaultTexture;
                }
                break;

            default:
                break;
        }
    }
    private void CreateTileMap()
    {
        TileMap tempMap = GetTileMap();

        string path = $"Assets/Resources/ScrObjects/TileMaps/{newTileMapName}.asset";

        try
        {
            AssetDatabase.CreateAsset(tempMap, path);
        }
        catch (System.Exception e)
        {
            EditorUtility.DisplayDialog("Error", e.Message, "OK");
            return;
        }
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }

    private void ExportTileMap()
    {
        TileMap tempMap = GetTileMap();
        string json = "";
        foreach (var tile in tempMap.tiles)
        {
            json += $"\"{tile.ClassName}\": ";
            json += JsonUtility.ToJson(new TileStructJson(tile)) + "\n";
        }

        File.WriteAllText($"Assets/Resources/JSON/{newTileMapName}.json", json);
    }

    private TileMap GetTileMap()
    {
        TileMap tempMap = new TileMap();
        tempMap.tiles = new List<TileStruct>();

        foreach (var tile in tileMapDict)
        {
            tempMap.tiles.Add(new TileStruct(tile.Value));
        }

        return tempMap;
    }

    private void LoadTileMap()
    {
        if (loadedTileMap != null)
        {
            for (int i = 0; i < tileMapDict.Count; i++)
            {
                var pair = tileMapDict.ElementAt(i);
                tileMapDict[pair.Key] = new Tile(loadedTileMap.tiles[i]);
                (pair.Key.Children().First() as Image).image = tileMapDict[pair.Key].texture;
            }
        }
    }

    private void CreateNewTile()
    {
        Tile tile = CreateInstance<Tile>();

        if (newTileName != "")
            tile.name = newTileName;
        else
            tile.name = "defaultName";

        if (newTileTexture != null)
            tile.texture = newTileTexture;
        else
            tile.texture = defaultTexture;

        if (newTileClassName != "")
            tile.className = newTileClassName;
        else
            tile.className = "Tile";

        tile.blocksMovement = newTileBlocksMovement;

        string path = $"Assets/Resources/ScrObjects/Tiles/{tile.name}.asset";

        try
        {
            AssetDatabase.CreateAsset(tile, path);
        }
        catch (System.Exception e)
        {
            EditorUtility.DisplayDialog("Error", "Error creating new asset. Maybe it's already exists?", "OK");
            return;
        }
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();

        tiles.Add(tile);
        tileSelection.Rebuild();
    }
}
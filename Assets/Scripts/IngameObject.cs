using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "IngameObject", menuName = "Editor Data/Ingame Object")][System.Serializable]
public class IngameObject : ScriptableObject
{
    public string className;
    public Texture2D texture;
    public bool blocksMovement = false;
    public Vector2Int position;

    private void Awake()
    {
        if (className == null)
            className = "Object";
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Character", menuName = "Editor Data/Character")][System.Serializable]
public class Character : IngameObject
{
    public int maxHealth;
    public int health;
    public bool dead;

    private void Awake()
    {
        if (className == null)
            className = "Character";
    }
}

[System.Serializable]
public struct CharStruct
{
    public CharStruct(Character _char)
    {
        Name = _char.name;
        ClassName = _char.className;
        Texture = _char.texture;
        BlocksMovement = _char.blocksMovement;
        Position = _char.position;
        MaxHealth = _char.maxHealth;
        Health = _char.health;
        Dead = _char.dead;
    }

    public string Name;
    public string ClassName;
    public Texture2D Texture;
    public bool BlocksMovement;
    public Vector2Int Position;
    public int MaxHealth;
    public int Health;
    public bool Dead;
}

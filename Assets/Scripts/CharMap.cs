using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CharMap", menuName = "Editor Data/Character Map")]
public class CharMap : ScriptableObject
{
    public List<CharStruct> characters;
}

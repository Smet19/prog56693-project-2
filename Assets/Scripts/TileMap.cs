using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "TileMap", menuName = "Editor Data/Tile Map")]
public class TileMap : ScriptableObject
{
    public List<TileStruct> tiles;
}

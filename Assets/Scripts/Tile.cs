using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "Tile", menuName = "Editor Data/Tile")][System.Serializable]
public class Tile : IngameObject
{
    private void Awake()
    {
        if (className == null)
            className = "Tile";
    }
    public Tile() { }

    public Tile(TileStruct _tileStruct)
    {
        name = _tileStruct.Name;
        className = _tileStruct.ClassName;
        texture = _tileStruct.Texture;
        blocksMovement = _tileStruct.BlocksMovement;
        position = _tileStruct.Position;
    }
}

[System.Serializable]
public struct TileStruct
{
    public TileStruct(Tile _tile)
    {
        Name = _tile.name;
        ClassName = _tile.className;
        Texture = _tile.texture;
        BlocksMovement = _tile.blocksMovement;
        Position = _tile.position;
    }

    public string Name;
    public string ClassName;
    public Texture2D Texture;
    public bool BlocksMovement;
    public Vector2Int Position;
}

[System.Serializable]
public struct TileStructJson
{
    public TileStructJson(TileStruct _tile)
    {
        Name = _tile.Name;
        ClassName = _tile.ClassName;
        TexturePath = AssetDatabase.GetAssetPath(_tile.Texture);
        BlocksMovement = _tile.BlocksMovement;
        Position = _tile.Position;
    }

    public string Name;
    public string ClassName;
    public string TexturePath;
    public bool BlocksMovement;
    public Vector2Int Position;
}
